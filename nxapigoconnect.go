package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/url"

	"bitbucket.org/cgrande/nxapiutils"
)

func main() {
	var body []byte
	var response *http.Response
	var request *http.Request

	// No need to set as const since cannot be evaluated at compile time if passed as CLI args
	version := "1.0"
	cliType := "cli_show"
	chunk := "0"
	sid := "1"
	cmd := "show ip route 10.1.2.2/32 ; show clock"
	outputFormat := "json"

	//initialise with an empty struct
	r := nxapiutils.NxapiRequest{}

	user := "admin"
	pwd := "123Cisco123"
	nxapiUrl := "http" + "://" + "10.67.182.13" + "/ins/"

	// Send input parameter
	r.SetNxapiRequest(version, cliType, chunk, sid, cmd, outputFormat)
	// get marshalled json request
	requestParams := r.JsonNxapiRequest()

	// don't set options such as public suffix by placing nil inside new()
	cookieJar, err := cookiejar.New(nil)
	if err != nil {
		log.Fatalf("Error initialising the cookieJar: %s", err)
	}

	// URL for cookies to remember => reply with cookie when encounter nxapiUrl
	cookieUrl, err := url.Parse(nxapiUrl)
	if err != nil {
		log.Fatalf("Error parsing the cookie URL: %s", err)
	}

	// don't use GET otherwise you'll get a 405 return code as the method is not allowed
	// bytes buffer is there to satisfy the io.reader interface but is there another way to do that instead of sending a byte slice?
	request, err = http.NewRequest("POST", nxapiUrl, bytes.NewBuffer(requestParams))
	if err != nil {
		log.Fatalf("Error forming http request: %s", err)
	}
	request.SetBasicAuth(user, pwd)
	request.Header.Add("Content-Type", "application/json")

	//create an http client for control over client header, cookies, etc.
	//
	// also create a "transport" that gets passed on to the client if need to control TLS, proxies, compression, etc.
	nxapiClient := &http.Client{Jar: cookieJar}

	response, err = nxapiClient.Do(request)
	if err != nil {
		log.Fatalf("Error posting http request: %s", err)
	}

	// defer closes the body of the response until the client is finished with it (e.g. main exits)
	defer response.Body.Close()
	body, err = ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatalf("Error reading the body of the response: %s", err)
	}

	//check that cookie is not present in initial request
	fmt.Printf("The request was:\n %v \n", request)
	//view body of response
	fmt.Printf("The Body of response was:\n %s \n", body)
	//sanity check cookie
	fmt.Printf("\n\n The content of the cookiejar for %s is: %v \n", cookieUrl, cookieJar.Cookies(cookieUrl))

	/*
		//
		// second request to verify cookie can be reused
		// remove ":" from variable assigment to reuse existing ones
		//

		r.SetNxapiRequest(version, cliType, chunk, sid, "show clock", outputFormat)
		requestParams = r.NxapiRequest()
		//requestParams = nxapiutils.SetNxapiRequest(version, cliType, chunk, sid, "show clock", outputFormat)

		// marshal the data to format as nested json
		js, err = json.Marshal(requestParams)
		if err != nil {
			log.Fatalf("Error marshalling the json data: %s", err)
		}

		// NewRequest needs type that implements io.reader as argument
		requestBody = bytes.NewBuffer(js)

		// don't use GET otherwise you'll get a 405 return code as the method is not allowed
		request, err = http.NewRequest("POST", nxapiUrl, requestBody)

		// if the cookieJar isn't added when called http.Client{}
		// then you get "HTTP/1.1 401 Unauthorized"
		nxapiClient = &http.Client{Jar: cookieJar}
		if err == nil {
			request.Header.Add("Content-Type", "application/json")
			debug(httputil.DumpRequestOut(request, debugmode))
			response, err = nxapiClient.Do(request)
		}

		if err == nil {
			defer response.Body.Close()
			debug(httputil.DumpResponse(response, debugmode))
			body, err = ioutil.ReadAll(response.Body)
		}

		if err == nil {

			//check that cookie is reused in second request
			fmt.Printf("%v", request)
			//view body of response
			fmt.Printf("%s", body)

			//sanity check cookie
			fmt.Printf("\n\n The content of the cookiejar for %s is: %v \n", cookieUrl, cookieJar.Cookies(cookieUrl))
		} else {
			log.Fatalf("ERROR: %s", err)
		}
	*/
}
